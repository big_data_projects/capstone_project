# Building a scalable and reliable ETL pipeline using Big Data technology

The project is aim to a create ETL with stream processing.
The design should take into consideration:
- Scalability
- Fault Tolerance

## Setup
1. Add AWS credentials to .aws/credentials in section studia
2. Run command from main directory
```
bash config.sh
```
3. Inside the docker/

```
docker-compose up -d --build
```

```
docker-compose ps
```

4. Creating the kafka topic

After downloading [kafka binary](https://kafka.apache.org/downloads), the topic can be created in containerized  kafka kluster 
```
bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 2 --partitions 3 --topic AWSKafkaTwitter
```
5. Creating AWS EMR cluster. => TO DO IaaS


6. Create variables in Airflow

* BUCKET_NAME 
* CLUSTER_ID
* KAFKA_HOST_PORT 
* KAFKA_TOPIC 
* TWITTER_TRACKS 

7. Create aws_connection in Airflow


## Main flow

The ETL will run using Apache Airflow.

The main dag is streaming_pipeline. The streaming_pipeline triggers the twitter_streaming dag.


![Airflow_dags](/resources/dags.png )

streaming_pipeline
![Airflow_dags](/resources/airflow_streaming.png )
twitter_streaming
![Airflow_dags](/resources/twitter_streaming.png )
The dag scrips are [streaming_pipeline](dags/pipeline_dag.py) and [twitter_streaming](dags/twitter_streaming_dags.py)

1. Extract data into Kafka from Twitter
Apache kafka is a publish and subscribe messaging system which contains producers and consumers.
Topic is a way of labeling groups of messages.
* Producers:Sends messages to Kafka
* Consumer:Retrieves messages from Kafka
* Stream Processor:Producing messages into output streams
* Connector: Connecting topics to existing applications

Kafka cluster container:
* Zookeepers ports: 2181-83
* Kafka brokers ports: 9092-94
* References: https://github.com/linuxacademy/content-kafka-deep-dive.git


2. Transfer data using Spark Streaming

* spark: Spark Master.
    * Image: bitnami/spark:3.1.2
    * Port: 8181
    * References: 
      * https://github.com/bitnami/bitnami-docker-spark
      * https://hub.docker.com/r/bitnami/spark/tags/?page=1&ordering=last_updated

* spark-worker-N: Spark workers. You can add workers copying the containers and changing the container name inside the docker-compose.yml file.
    * Image: bitnami/spark:3.1.2
    * References: 
      * https://github.com/bitnami/bitnami-docker-spark
      * https://hub.docker.com/r/bitnami/spark/tags/?page=1&ordering=last_updated

3. Load data to Cassandra

*cassandra cluster
    * image: docker.io/bitnami/cassandra:4.0
    * Port: 9042-44
    * References: https://hub.docker.com/r/bitnami/cassandra


4. Orchestration

* Airflow
    * Port: 8080
    * References: https://airflow.apache.org/docs/apache-airflow/stable/start/docker.html




### Architecture components local

![Diagram](/resources/kappa_update.png )

## AWS potential setup

For production deployment, the AWS services could be utilized suach as:

* ECS 
    * References: https://aws.amazon.com/ecs/

* MSK 
    * References: https://aws.amazon.com/msk/

* EMR 
    * References: https://aws.amazon.com/emr/

* Amazon Keyspaces 
    * References: https://aws.amazon.com/keyspaces/

* Amazon Managed Workflows for Apache Airflow (MWAA)
    * References: https://aws.amazon.com/keyspaces/


### Architecture components AWS

![AWS_Diagram](/resources/aws_kappa.png )
