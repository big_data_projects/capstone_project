#! bin/bash

# if [[ $# -eq 0 ]] ; then
#     echo 'Please enter your bucket name '
#     exit 0
# fi

# echo "Creating bucket "$1""
aws s3api --profile studia create-bucket --acl public-read-write --bucket "spark-bigdata-studia-2022" --output text >> setup.log

cd ./dags/src/spark/
export SPARK_BUCKET="spark-bigdata-studia-2022"

aws s3 --profile studia cp emr_jars/ \
  "s3://${SPARK_BUCKET}/jars/" --recursive
aws s3 --profile studia cp scripts/ \
  "s3://${SPARK_BUCKET}/pyspark/" --recursive