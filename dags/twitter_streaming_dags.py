from airflow import DAG
import os
import sys
sys.path.append('/opt/airflow/dags/modules') #
import time

import airflow
from airflow.models import Variable
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python import PythonOperator
from datetime import datetime, timedelta

from src.kafka.streaming import twitter_streaming
from src.utils import datetime_utils

#######################################
# Variables
#######################################
SCRIPT_DIR = os.path.dirname(__file__)
KAFKA_HOST_PORT  = Variable.get("KAFKA_HOST_PORT")
KAFKA_TOPIC = Variable.get("KAFKA_TOPIC")
TWITTER_TRACKS = Variable.get("TWITTER_TRACKS")

#######################################
# DEFAULT_ARGS
#######################################
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(1),
    'email': ['tmatczak@gmail.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1)
}

#######################################
# Python Function
#######################################
def start_task(**kwargs):
    """
    Set timer at the begining of the task and push global_start_time using xcom
    """
    global_start_time = time.time()
    kwargs['ti'].xcom_push(key="task_key", value=global_start_time)
    print("\n    Start time:", global_start_time)

def end_task(**kwargs):
    """
    Pull global_start_time using xcom and print out the elapsed time. 
    """
    g_st_tm = kwargs.get('ti').xcom_pull(key="task_key")
    elapsed_time = time.time() - g_st_tm
    datetime_utils.print_elapsed_time(elapsed_time)

#######################################
# DAG and Tasks
#######################################

with DAG('twitter_streaming',
        default_args=default_args,
        description='Stream data from Twitter to Kafka',
        tags=['twitter'],
        schedule_interval=None) as dag:
    
    start = PythonOperator(task_id="start",
                            python_callable=start_task,
                            provide_context=True
                        )

    streaming = PythonOperator(
                        task_id="from_twitter_to_kafka",
                        python_callable=twitter_streaming,
                        op_kwargs={
                            "host_port": KAFKA_HOST_PORT, #inside docker => kafka1:19092
                            "topic": KAFKA_TOPIC, #"AWSKafkaTwitter"
                            "tracks": TWITTER_TRACKS # ["#Hadoop","#Python","#BigData","#Spark","#AWS","#ApacheAirflow","#ApacheCassandra","#ApacheKafka"]
                        }
                    )

    end = PythonOperator(
                            task_id="end",
                            python_callable=end_task
                        )

#######################################
# Dependencies
#######################################

start >> streaming >> end