from airflow import DAG
import os
import sys
sys.path.append('/opt/airflow/dags/src')
import time
from timeit import default_timer as timer
import airflow
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
from src.utils import datetime_utils
from src.kafka.streaming import twitter_streaming
from src.spark.scripts.test import spark_driver_host
#from airflow.providers.apache.spark.operators.spark_submit import SparkSubmitOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from airflow.contrib.operators.ssh_operator import SSHOperator
# host_port = "b-1.kafkacluster.ff5k45.c21.kafka.us-east-1.amazonaws.com:9092"
# topic = "AWSKafkaTwitter"
# tracks = ["#Hadoop","#Python","#BigData"]

SCRIPT_DIR = os.path.dirname(__file__)
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(1),
    'email': ['tmatczak@gmail.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1)
}


#######################################
# Python Function
#######################################
def start_task(**kwargs):
    """
    Set timer at the begining of the task and push global_start_time using xcom
    """
    global_start_time = time.time()
    kwargs['ti'].xcom_push(key="task_key", value=global_start_time)
    print("\n    Start time:", global_start_time)

def end_task(**kwargs):
    """
    Pull global_start_time using xcom and print out the elapsed time.
    """
    g_st_tm = kwargs.get('ti').xcom_pull(key="task_key")
    elapsed_time = time.time() - g_st_tm
    datetime_utils.print_elapsed_time(elapsed_time)

templated_bash_command = """
    echo 'USER: $USER' #To check that you are properly connected to the host
    {{ params.spark_submit }} --master {{ params.master }} {{ params.script }}
"""




with DAG('spark_submit_docker',default_args=default_args,description='Our first DAG with ETL process!',schedule_interval=None,
             params={'spark_submit': '/opt/spark/bin/spark-submit',
                  'master': 'spark://0.0.0.0:7077',
                  'script': '/home/ubuntu/spark_test.py'}) as dag:

    start = PythonOperator(task_id="start",
                            python_callable=start_task
                        )


    spark_submit = SparkSubmitOperator(
                    task_id='spark_submit_job',
                    application=SCRIPT_DIR+'/src/spark/scripts/spark_test.py',
                    conn_id='spark_ec2',
                    conf={"spark.master":"spark://0.0.0.0:7077"},
                    verbose=1)
    #                 #packages='org.apache.spark:spark-sql-kafka-0-10_2.12:3.1.2',
    #                 #jars="/opt/bitnami/spark/jars/spark-sql-kafka-0-10_2.12-3.1.2.jar,/opt/bitnami/spark/jars/spark-cassandra-connector_2.12-3.1.0.jar",
    #                 # conf={"deployMode":"cluster"},
    #                 # conf={"spark.driver.blockManager.port", "10026"},
    #                 # conf={"spark.driver.port", "10027"},
    #                 #conf=("spark.executor.memory", "2g"),
    #                 #conf={"spark.master":"local"},
    #                 #conf={"spark.master":"spark://tmatczak-VirtualBox:7077"},
    #                 #conf={"spark.master":"spark://tmatczak-VirtualBox:7077"},
    #                 #conf={"deployMode":"cluster","spark.master":"spark://localhost:7077"},
    #                 #conf={"spark.master":"spark://localhost:6066"},
    #                 #com.datastax.spark:spark-cassandra-connector_2.12:3.1.0,
    #                 )            
    
    # spar_submit_ssh1 = SSHOperator(
    #                 task_id="SSH_task1",
    #                 ssh_conn_id='ssh_ec2',
    #                 command='echo $USER'
    #                 )

    # spar_submit_ssh2 = SSHOperator(
    #                 task_id="SSH_task2",
    #                 ssh_conn_id='ssh_ec2',
    #                 command=templated_bash_command
    #                 )

                        
    end = PythonOperator(
                        task_id="end",
                        python_callable=end_task,
                        op_kwargs={
                        }             
        )

start >> spark_submit >> end

# spar_submit_ssh = SSHOperator(
#                     task_id="SSH_task",
#                     ssh_conn_id='ssh_spark_master',
#                     command=templated_bash_command,
#                     dag=dag)
# t = PythonOperator(
#                     task_id="test",
#                     python_callable=spark_driver_host,
#                     op_kwargs={
#                     },dag=dag
                                    
#         )

    # streaming = PythonOperator(
    #                     task_id="kafka_producer",
    #                     python_callable=twitter_streaming,
    #                     op_kwargs={
    #                         "host_port": "kafka1:19092", #kafka1:19092
    #                         "topic":"AWSKafkaTwitter",
    #                         "tracks": ["#Hadoop","#Python","#BigData"]
    #                     })
    

    # spar_submit_ssh = SSHOperator(
    #                 task_id="SSH_task",
    #                 ssh_conn_id='ssh_spark_master',
    #                 command=templated_bash_command,
    #                 dag=dag)

# spark_submit = SparkSubmitOperator(
#     task_id='spark_submit_job',
#     application=SCRIPT_DIR+'src/spark/spark.py',
#     packages='org.apache.hadoop:hadoop-aws:3.2.0',
#     conn_id='local_spark',
#     #conf={"spark.master":"spark://tmatczak-VirtualBox:7077"},
#     conf={"spark.master":"spark://spark-master:7077"},
#     #conf={"spark.master":"spark://localhost:6066"},
#     name='airflow-spark',
#     verbose=1,
#     dag=dag,
# )

# task1 = BashOperator(
#         task_id='spark_bash_operator',
#         bash_command='spark-submit --master {{params.master}} {{params.pyfile}}',
#         params={'master':'spark://tmatczak-VirtualBox:7077',
#                 'pyfile':SCRIPT_DIR+'/spark.py'},
#         dag=dag
# )

# task1 = BashOperator(
#         task_id='spark_bash_operator',
#         bash_command='python {{params.pyfile}}',
#         params={'master':'spark://tmatczak-VirtualBox:7077',
#                 'pyfile':SCRIPT_DIR+'/spark.py'},
#         dag=dag
# )

