from subprocess import check_output

def spark_driver_host():

    SPARK_DRIVER_HOST = check_output(["hostname", "-i"]).decode(encoding="utf-8").strip()
    print(SPARK_DRIVER_HOST)

