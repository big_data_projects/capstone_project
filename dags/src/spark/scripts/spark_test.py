from pyspark.sql import SparkSession
from pyspark.sql.types import StructType,StringType, StructField, StringType, IntegerType
import sys, os
from pyspark.conf import SparkConf
from subprocess import check_output

def main():
    """
    Test spark submit

    """
    spark_conf = SparkConf()

    # SPARK_DRIVER_HOST = check_output(["hostname", "-i"]).decode(encoding="utf-8").strip()
    # print(SPARK_DRIVER_HOST)
    # spark_conf.setAll(
    # [
    #     (
    #         "spark.master",
    #         "spark://spark-master:7077",
    #     ),  # <--- this host must be resolvable by the driver in this case pyspark (whatever it is located, same server or remote) in our case the IP of server
    #     ("spark.app.name", "myApp"),
    #     ("spark.submit.deployMode", "client"),
    #     ("spark.ui.showConsoleProgress", "true"),
    #     ("spark.eventLog.enabled", "false"),
    #     ("spark.logConf", "false"),
    #     (
    #         "spark.driver.bindAddress",
    #         "3.239.35.240",
    #     ),  # <--- this host is the IP where pyspark will bind the service running the driver (normally 0.0.0.0)
    #     (
    #         "spark.driver.host",
    #         SPARK_DRIVER_HOST,
    #     ),  # <--- this host is the resolvable IP for the host that is running the driver and it must be reachable by the master and master must be able to reach it (in our case the IP of the container where we are running pyspark
    # ]
    # )


    spark = SparkSession.builder.master("localhost:7077").appName("test").getOrCreate()
    
    schema = StructType([StructField("name",StringType(),True),StructField("id",IntegerType(),True)])
    data =[('Test1',1),('Test2',2)]
    df = spark.createDataFrame(data,schema=schema)
    print(df.show())

# Starting point for PySpark
if __name__ == '__main__':
    main()
