"""
The module for twitter data transformations
"""

from pyspark.sql import functions as f


def convert_binary_value_to_string(raw,schema):
    """Transform twitter data.
        1. Read data from json

    Args:
        raw (string): The twitter data in json format.
        schema (pyspark.sql.types.StructType): The schema for twitter data.

    Returns:
        [pyspark.sql.DataFrame]: The df converted to string with correct schema.
    """
    df_transform = raw.select(f.from_json(raw.value.cast("string"),schema).alias("json"))\
    
    return df_transform

def transform_tweet_data(df_transform):
    """Transform twitter data.
        1. Read data from json

    Args:
        df_transform (pyspark.sql.DataFrame): The twitter data.

    Returns:
        [pyspark.sql.DataFrame]: The tweet twitter data.
    """


    tweet = df_transform \
            .withColumn("timestamp",f.col("json").getField("created_at"))\
            .select(
                f.col("json").getField("tweet_id").alias("tweet_id"),
                f.col("json").getField("retweetcount").cast("integer").alias("retweet_count"),
                f.col("json").getField("favorite_count").cast("integer").alias("favorite_count"),
                f.col("json").getField("tweet").alias("text"),"timestamp"
                )

    return tweet

def transform_user_data(df_transform):
    """Transform twitter data.
        1. Read data from json
        2. Filter out user_id

    Args:
        df_transform (pyspark.sql.DataFrame): The twitter data.


    Returns:
        [pyspark.sql.DataFrame]: The user twitter data.
    """

    user = df_transform\
            .select(
                f.col("json").getField("user_id").cast("long").alias("user_id"),
                f.col("json").getField("followers_count").cast("integer").alias("followers_count"),
                f.col("json").getField("friends_count").cast("integer").alias("friends_count"),
                f.col("json").getField("location").alias("location"),
                f.col("json").getField("user_name").alias("name")).where(f.col("user_id") > 0
                )

    return user
