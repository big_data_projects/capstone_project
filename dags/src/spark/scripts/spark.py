"""Main spark_jobs
"""
import os
# !ls /usr/local/spark/jars/hadoop* # to figure out what version of hadoop
#os.environ['PYSPARK_SUBMIT_ARGS'] = '--packages "org.apache.hadoop:hadoop-aws:3.2.0" pyspark-shell'


from pyspark.sql import SparkSession
from pyspark.sql.types import StructType,StringType, StructField, IntegerType,LongType
import spark_transformation
#from sparkspark_transformation. import transform_tweet_data, transform_user_data # pylint: disable=import-error

SCRIPT_DIR = os.path.dirname(__file__)


def save_data_to_cassandra(kafka_server="ec2-3-218-152-159.compute-1.amazonaws.com:9092",
                            kafka_topic='AWSKafkaTwitter',
                            cassandra_host="ec2-3-235-1-170.compute-1.amazonaws.comec2-3-235-1-170.compute-1.amazonaws.com:9042"):
    """Function read twitter data from kafka, transform it and save it into cassandra database.

    Args:
        kafka_server (str): Kafka server. Defaults to "kafka1:19092,kafka2:19093,kafka3:19094".
        kafka_topic (str): Kafka topic. Defaults to 'AWSKafkaTwitter'.
        cassandra_host (str): cassandra server. Defaults to "cassandra".
    """
        # Create Spark Session
        # spark = pyspark.sql.SparkSession.builder \
        #                   .appName("Test_parquet") \
        #                   .master("spark://tmatczak-VirtualBox:7077") \
        #                   .config('spark.jars.packages', 'org.apache.hadoop:hadoop-aws:3.2.0') \
        #                   .getOrCreate()

    spark = SparkSession\
        .builder\
        .appName("Twitter")\
        .config("spark.cassandra.connection.host", cassandra_host)\
        .config("spark.cassandra.auth.username", "cassandra")\
        .config("spark.cassandra.auth.password", "cassandra")\
        .getOrCreate()
        #.config("spark.jars", s3_path"/spark-cassandra-connector_2.12-3.1.0.jar")\
        #.config("spark.jars", s3_path"/cassandra-driver-core-3.9.0.jar")\
        #.config("spark.sql.extensions", "com.datastax.spark.connector.CassandraSparkExtensions") \
        #.config("spark.sql.catalog.cass100", "com.datastax.spark.connector.datasource.CassandraCatalog") \




    raw = spark.readStream\
                .format("kafka")\
                .option("kafka.bootstrap.servers", kafka_server)\
                .option("subscribe", kafka_topic)\
                .load()

    raw.printSchema()

    schema = StructType([StructField('created_at',StringType()),
                    StructField('tweet_id',LongType()),
                    StructField('retweetcount',IntegerType()),
                    StructField('favorite_count',IntegerType()),
                    StructField('tweet',StringType()),
                    StructField('place',StringType()),
                    StructField('user_id',LongType()),
                    StructField('user_name',StringType()),
                    StructField('followers_count',IntegerType()),
                    StructField('friends_count',IntegerType()),
                    StructField('location',StringType())])

    

    df_tweet = spark_transformation.convert_binary_value_to_string(raw,schema)
    tweet = spark_transformation.transform_tweet_data(df_tweet)
    df_user = spark_transformation.convert_binary_value_to_string(raw,schema)
    user = spark_transformation.transform_user_data(df_user)

    query = tweet.writeStream \
        .format("org.apache.spark.sql.cassandra") \
        .options(table="tweets", keyspace="twitter") \
        .outputMode("append") \
        .option("checkpointLocation", 'check_point_t')\
        .start()

    query = user.writeStream \
        .format("org.apache.spark.sql.cassandra") \
        .options(table="users", keyspace="twitter") \
        .outputMode("append") \
        .option("checkpointLocation", 'check_point_u')\
        .start()

    query.awaitTermination(60)
    query.stop()

def kafka_test(kafka_server="ec2-3-218-152-159.compute-1.amazonaws.com:9092",
                kafka_topic='AWSKafkaTwitter'):
    """Function read twitter data from kafka.

    Args:
        kafka_server (str): Kafka server. Defaults to "kafka1:19092,kafka2:19093,kafka3:19094".
        kafka_topic (str): Kafka topic. Defaults to 'AWSKafkaTwitter'.
    """

    spark = SparkSession\
        .builder\
        .appName("Twitter") \
        .getOrCreate()

    schema = StructType([StructField('created_at',StringType()),
                            StructField('tweet_id',LongType()),
                            StructField('retweetcount',IntegerType()),
                            StructField('favorite_count',IntegerType()),
                            StructField('tweet',StringType()),
                            StructField('place',StringType()),
                            StructField('user_id',LongType()),
                            StructField('user_name',StringType()),
                            StructField('followers_count',IntegerType()),
                            StructField('friends_count',IntegerType()),
                            StructField('favourites_count',IntegerType()),
                            StructField('location',StringType())])

    raw = spark.readStream\
                .format("kafka")\
                .option("kafka.bootstrap.servers", kafka_server)\
                .option("subscribe", kafka_topic)\
                .load()

    raw.printSchema()

    df_tweet = spark_transformation.convert_binary_value_to_string(raw,schema)
    tweet = spark_transformation.transform_tweet_data(df_tweet)
    df_user = spark_transformation.convert_binary_value_to_string(raw,schema)
    user = spark_transformation.transform_user_data(df_user)


    query = tweet.writeStream\
                .outputMode("append")\
                .format("console")\
                .start()

    query = user.writeStream\
            .outputMode("append")\
            .format("console")\
            .start()

    query.awaitTermination(60)
    query.stop()



# Main program
if __name__ == "__main__":

    kafka_test()
