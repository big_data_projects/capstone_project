"""This script creates a twitter stream and send it to kafka broker

Args:
    kafka_host_port = The kafka host address and port. For local dev/test localhost:9092
    kafka_topic = The Kafka topic. AWSKafkaTwitter
    tweet_tracks = The Twitter hashtag to filter stream.
"""
import sys
import os
import json
from datetime import datetime
import logging
from tweepy import OAuthHandler
import tweepy
import pykafka
sys.path.append('/home/tmatczak/Desktop/python/capstone_project/dags/src')
sys.path.append('/opt/airflow/dags/src')
from utils.helper import read_config

c = os.path.dirname(__file__)
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

class TweetsListener(tweepy.Stream):
    """The class will filter and sample realtime Tweets using Twitter's API
    """
    def __init__(self,api_key,api_secret,access_token,access_token_secret, kafka_producer):
        super().__init__(api_key,api_secret,access_token,access_token_secret)

        self.tweet_count = 0
        logger.info("_______Tweets producer initialized_______")
        self.producer = kafka_producer

    def on_data(self, raw_data):
        """Produce a byte steam data

        Args:
            raw_data (string): The raw dta from the stream in JSON format.

        Returns:
            [byt]: Produce a byte stream with Twitter data in
        """
        try:
            tweet_dict = {}
            json_data = json.loads(raw_data)

            # tweet
            tweet_dict['created_at'] = datetime.strftime(
                    datetime.strptime(json_data['created_at'],'%a %b %d %H:%M:%S +0000 %Y')
                                , '%Y-%m-%d %H:%M:%S')
            tweet_dict['tweet_id'] = json_data['id']
            tweet_dict['retweetcount'] = int(json_data['retweet_count'])
            tweet_dict['favorite_count'] = int(json_data['favorite_count'])
            tweet_dict['tweet'] = json_data["text"]
            if json_data['place'] is not None:
                tweet_dict['place'] = json_data['place']['country']
            else:
                tweet_dict['place'] = None
            # user
            tweet_dict['user_id'] = json_data['user']['id']
            tweet_dict['user_name'] = json_data['user']['screen_name']
            tweet_dict['followers_count'] = json_data['user']['followers_count']
            tweet_dict['friends_count'] = json_data['user']['friends_count']
            tweet_dict['favourites_count'] = json_data['user']['favourites_count']
            tweet_dict['location'] = json_data['user']['location']

            # current_time = datetime.now()
            # print(current_time.strftime("%Y%m%d%H%M%S%f"))
            # json_name = current_time.strftime("%Y%m%d%H%M%S%f")

            # with open(f"{SCRIPT_DIR}/json/tweet_{json_name}.json","w",encoding="utf-8") as f:
            #     json.dump(tweet_dict,f,ensure_ascii=False,indent=4)

            self.producer.produce(bytes(json.dumps(tweet_dict), "utf-8"))

        except KeyError as e:
            logger.error(f"Error on_data: {e}")

        self.tweet_count+=1

        if self.tweet_count > 100:
            logger.info("Max num reached = " + str(self.tweet_count))
            self.disconnect()
        else:
            return True


    def on_request_error(self, status):

        logger.info(status)

        return True

def connect_to_twitter(api_key,api_secret,access_token,access_token_secret,kafka_producer, tracks):
    """Connect to Twitter

    Args:
        api_key (str): api_key
        api_secret (str): api_secret
        access_token (str): access_token
        access_token_secret (str): access_token_secret
        kafka_producer (): kafka_producer
        tracks (list): Twitter hashtag values
    """

    auth = OAuthHandler(api_key, api_secret)
    auth.set_access_token(access_token, access_token_secret)

    twitter_stream = TweetsListener(
                                    api_key,
                                    api_secret,
                                    access_token,
                                    access_token_secret,
                                    kafka_producer)

    twitter_stream.filter(track=tracks, languages=["en"])

def twitter_streaming(host_port,topic,tracks):
    """This function validate json with json schema.

    Args:
      host_port (str): A stock stock_name.
      topic (str): The json for the validation.
      tracks (str): The path to the json schema.

    """
    client = pykafka.KafkaClient(host_port)

    producer = client.topics[bytes(topic, "utf-8")].get_producer()

    config = read_config()
    t_cfg = config['Twitter']
    connect_to_twitter(t_cfg['api_key'],
                    t_cfg['api_secret'],
                    t_cfg['access_token'],
                    t_cfg['access_token_secret'],
                    producer,
                    tracks)


if __name__ == "__main__":

    if len(sys.argv) < 4:
        print("Usage: python kafka_tweets_producer.py <host:port> <topic_name> <tracks>",
                file=sys.stderr)

        exit(-1)

    kafka_host_port = sys.argv[1]
    kafka_topic = sys.argv[2]
    tweet_tracks = sys.argv[3:]

    kafka_client = pykafka.KafkaClient(kafka_host_port)

    kafka_producer = kafka_client.topics[bytes(kafka_topic, "utf-8")].get_producer()

    cfg = read_config()
    twitter_cfg = cfg['Twitter']
    connect_to_twitter(twitter_cfg['api_key'],
                    twitter_cfg['api_secret'],
                    twitter_cfg['access_token'],
                    twitter_cfg['access_token_secret'],
                    kafka_producer,
                    tweet_tracks)
