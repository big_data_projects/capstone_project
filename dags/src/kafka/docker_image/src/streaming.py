import sys
import tweepy
import json
import os
from tweepy import OAuthHandler
from datetime import datetime
import json
import pykafka
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


class TweetsListener(tweepy.Stream):
    """The class will filter and sample realtime Tweets using Twitter's API
    """
    def __init__(self,api_key,api_secret,access_token,access_token_secret, kafkaProducer):
        super().__init__(api_key,api_secret,access_token,access_token_secret)
        
        self.tweetCount=0
        
        #cons_key, cons_secret, token, token_sec
        logger.info("_______Tweets producer initialized_______")
        # self.consumer_key = consumer_key
        # self.consumer_secret = consumer_secret
        # self.access_token = access_token
        # self.access_token_secret = access_token_secret

        self.producer = kafkaProducer

    def on_data(self, data):
        """Produce a byte steam data

        Args:
            raw_data (string): The raw dta from the stream in JSON format.

        Returns:
            [byt]: Produce a byte stream with Twitter data in
        """
        try:
            tweet_dict = {}
            json_data = json.loads(data)

            # tweet
            tweet_dict['created_at'] = json_data['created_at']
            tweet_dict['tweet_id'] = int(json_data['id'])
            tweet_dict['retweetcount'] = int(json_data['retweet_count'])
            tweet_dict['favorite_count'] = int(json_data['favorite_count'])
            tweet_dict['tweet'] = json_data["text"]
            if json_data['place'] is not None:
                tweet_dict['place'] = json_data['place']['country']
            else:
                tweet_dict['place'] = None
            # user
            tweet_dict['user_id'] = json_data['user']['id_str']
            tweet_dict['user_name'] = json_data['user']['screen_name']
            tweet_dict['followers_count'] = json_data['user']['followers_count']
            tweet_dict['friends_count'] = json_data['user']['friends_count']
            tweet_dict['favourites_count'] = json_data['user']['favourites_count']
            tweet_dict['location'] = json_data['user']['location']


            self.producer.produce(bytes(json.dumps(tweet_dict), "utf-8"))

        except KeyError as e:
            print("Error on_data: %s" % str(e))

        self.tweetCount+=1

        return True

    def on_request_error(self, status):

        logger.info(status)

        return True

def connect_to_twitter(api_key,api_secret,access_token,access_token_secret,kafkaProducer, tracks):
    """Connect to Twitter

    Args:
        api_key (str): api_key
        api_secret (str): api_secret
        access_token (str): access_token
        access_token_secret (str): access_token_secret
        kafka_producer (): kafka_producer
        tracks (list): Twitter hashtag values
    """

    auth = OAuthHandler(api_key, api_secret)
    auth.set_access_token(access_token, access_token_secret)
    
    twitter_stream = TweetsListener(
                                    api_key,
                                    api_secret,
                                    access_token,
                                    access_token_secret,
                                    kafkaProducer)

    twitter_stream.filter(track=tracks, languages=["en"])


if __name__ == "__main__":

    API_KEY = os.getenv('API_KEY')
    API_SECRET = os.getenv('API_SECRET')
    ACCESS_TOKEN = os.getenv('ACCESS_TOKEN')
    ACCESS_TOKEN_SECRET = os.getenv('ACCESS_TOKEN_SECRET')
    BROKER = os.getenv('BROKER')
    KAFKA_TOPIC = os.getenv('KAFKA_TOPIC')

    tracks = ["#Hadoop","#Python","#BigData","#Spark","#AWS","#ApacheAirflow","#ApacheCassandra","#ApacheKafka"]

    kafkaClient = pykafka.KafkaClient(hosts=BROKER)

    kafkaProducer = kafkaClient.topics[bytes(KAFKA_TOPIC, "utf-8")].get_producer()

    connect_to_twitter( API_KEY,
                        API_SECRET,
                        ACCESS_TOKEN,
                        ACCESS_TOKEN_SECRET,
                        kafkaProducer,
                        tracks)
