#! bin/bash

bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 3 --topic topic1

bin/kafka-topics.sh --create --bootstrap-server b-1.kafkacluster.ff5k45.c21.kafka.us-east-1.amazonaws.com --replication-factor 1 --partitions 3 --topic AWSKafkaTwitter

# for docker
#kafka-topics --bootstrap-server kafka:29092 --create --if-not-exists --topic my-topic-1 --replication-factor 1 --partitions 1

aws kafka get-bootstrap-brokers \
    --cluster-arn arn:aws:kafka:us-east-1:864731172643:cluster/kafkaCluster/be9cd706-760e-4f88-a621-08c421390e80-21