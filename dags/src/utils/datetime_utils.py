"""Module with some datetime functions
"""
import datetime

def date_range(start_date,end_date):
    """
    Return list of the string.
    """
    sdate = datetime.datetime.strptime(start_date,"%Y-%m-%d")
    edate = datetime.datetime.strptime(end_date,"%Y-%m-%d")
    return [datetime.datetime.strftime(
            (sdate+datetime.timedelta(days=x)),"%Y-%m-%d")
            for x in range((edate-sdate).days)]

def print_elapsed_time(elapsed_time):
    """Create a human readable print out of elapsed time.

    Args:
        elapsed_time (int): Time
    """
    hours, rem = divmod(elapsed_time, 3600)
    minutes, seconds = divmod(rem, 60)
    print("\n    Total time spent: {:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds))

def datetime_to_str(date):
    """ Convert datetime to string in YYYY-MM-DD format"""

    return datetime.datetime.strftime(date,"%Y-%m-%d")

if __name__ =='__main__':

    date_range = date_range('2021-09-05','2021-09-07')
    print(date_range)
