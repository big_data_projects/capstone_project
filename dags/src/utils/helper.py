"""Helper functions for reading config file and aws credentials
"""
import configparser
import os
import json

SRC_PATH = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

def read_config():
    """
    The function that read the config.ini file

    Returns:
    The dictionary with config data.
    """

    file_path = os.path.join(SRC_PATH,'config','config.ini')
    config = configparser.ConfigParser()
    config.read(file_path)
    dictionary = {}
    for section in config.sections():
        dictionary[section] = {}
        for option in config.options(section):
            dictionary[section][option] = config.get(section, option)
    return dictionary

def get_twitter_key():
    """
    The function that read the config.ini file

    Returns:
    The dictionary with config data.
    """

def get_aws_credentials(aws_profile = "local"):
    """Get AWS credentials from .aws for specific profile.

    Args:
        aws_profile (str, optional): [description]. Defaults to "local".

    Returns:
        [type]: The AWs credentials
    """
    config = configparser.ConfigParser()
    config.read(os.path.expanduser("~/.aws/credentials"))
    access_id = config.get(aws_profile, "aws_access_key_id")
    access_key = config.get(aws_profile, "aws_secret_access_key")
    print(f'access_id: {access_id}')

    return access_id,access_key

def read_json_from_local_dir(folder_path,file_name):
  """Read json file to local dir.
  
  Args:
    folder_path (str): The path to folder, where file will be stored.
    file_name (str): The file name with extension.
  """
  json_path = os.path.join(folder_path,file_name)
  with open(json_path,'r',encoding='utf-8') as f:
      return json.loads(f.read())

if __name__ == "__main__":
    output = read_config()
    for k,v in output['Twitter'].items():
        print(k,v)
