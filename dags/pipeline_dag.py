
from datetime import timedelta

from airflow import DAG
from airflow.contrib.operators.emr_add_steps_operator import EmrAddStepsOperator
from airflow.contrib.sensors.emr_step_sensor import EmrStepSensor
from airflow.utils.dates import days_ago
from airflow.models import Variable
from airflow.contrib.operators.emr_terminate_job_flow_operator import (
    EmrTerminateJobFlowOperator,
)
from airflow.operators.trigger_dagrun import TriggerDagRunOperator

#######################################
# Variables
#######################################

CLUSTER_ID  = Variable.get("CLUSTER_ID")
BUCKET_NAME = Variable.get("BUCKET_NAME") #"spark-bigdata-studia-2022"
PY_FILES = "spark_transformation.py"
S3_PYSPARK_SCRIPT = "spark.py"


#######################################
# DEFAULT_ARGS
#######################################

DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['tmatczak@gmail.com'],
    'email_on_failure': False,
    'email_on_retry': False,
}


# add & edit the args as per your requirement. Change the pyspark file name.
SPARK_TASK = [
    {
        'Name': 'spark_app',
        'ActionOnFailure': 'CONTINUE',
        'HadoopJarStep': {
            'Jar': 'command-runner.jar',
            'Args': ["spark-submit",
                    "--deploy-mode",
                    "cluster",
                    "--jars",
                    "s3://{{ params.BUCKET_NAME }}/jars/*.jar", #"spark-cassandra-connector_2.12-3.1.0.jar,spark-sql-kafka-0-10_2.12-3.1.2.jar"
                    "--py-files",
                    "s3://{{ params.BUCKET_NAME }}/pyspark/{{ params.PY_FILES }}",
                    "s3://{{ params.BUCKET_NAME }}/pyspark/{{ params.S3_PYSPARK_SCRIPT }} \
                    > s3://{{ params.BUCKET_NAME }}/log.out 2>&1 &"],
        },
    }
]



#######################################
# DAG and Tasks
#######################################

with DAG(
    dag_id='streaming_pipeline',
    default_args=DEFAULT_ARGS,
    dagrun_timeout=timedelta(hours=2),
    start_date=days_ago(1),
    schedule_interval=None,
    tags=['streaming_pipeline'],
) as dag:

    # Trigger twitter_streaming
    trigger_twitter_extract = TriggerDagRunOperator(
                                task_id ='trigger_twitter',
                                trigger_dag_id = 'twitter_streaming'
    )

    # first step to register EMR step
    run_pyspark_on_EMR = EmrAddStepsOperator(
        task_id='add_emr_step',
        job_flow_id=CLUSTER_ID,
        aws_conn_id='aws_project',
        steps=SPARK_TASK,
        params={ 
        "BUCKET_NAME": BUCKET_NAME,
        "S3_PYSPARK_SCRIPT": S3_PYSPARK_SCRIPT,
        "PY_FILES": PY_FILES
    },
    )

    # second step to keep track of previous step.
    wait_for_sparkjob = EmrStepSensor(
        task_id='watch_emr_step',
        job_flow_id=CLUSTER_ID,
        step_id="{{ task_instance.xcom_pull(task_ids='add_emr_step', key='return_value')[0] }}",
        aws_conn_id='aws_project',
    )


    # Terminate the EMR cluster
    terminate_emr_cluster = EmrTerminateJobFlowOperator(
        task_id="terminate_emr_cluster",
        job_flow_id=CLUSTER_ID,
        aws_conn_id="aws_project"
    )
#######################################
# Dependencies
#######################################

    trigger_twitter_extract >> run_pyspark_on_EMR >> wait_for_sparkjob >> terminate_emr_cluster