#!bin/bash

#  docker run -d --name spark \
#   --network=airflow_default \
#   --hostname=spark \
#   -e SPARK_MODE=master \
#   -p 8181:8080 \
#   -p 7077:7077 \
#   bitnami/spark

 docker run -d --name spark \
  --hostname=spark \
  -e SPARK_MODE=master \
  -p 8181:8080 \
  -p 7077:7077 \
  bitnami/spark