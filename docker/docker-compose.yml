# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

# Basic Airflow cluster configuration for CeleryExecutor with Redis and PostgreSQL.
#
# WARNING: This configuration is for local development. Do not use it in a production deployment.
#
# This configuration supports basic configuration using environment variables or an .env file
# The following variables are supported:
#
# AIRFLOW_IMAGE_NAME           - Docker image name used to run Airflow.
#                                Default: apache/airflow:2.2.2
# AIRFLOW_UID                  - User ID in Airflow containers
#                                Default: 50000
# Those configurations are useful mostly in case of standalone testing/running Airflow in test/try-out mode
#
# _AIRFLOW_WWW_USER_USERNAME   - Username for the administrator account (if requested).
#                                Default: airflow
# _AIRFLOW_WWW_USER_PASSWORD   - Password for the administrator account (if requested).
#                                Default: airflow
# _PIP_ADDITIONAL_REQUIREMENTS - Additional PIP requirements to add when starting all containers.
#                                Default: ''
#
# Feel free to modify this file to suit your needs.
---
version: '3'

volumes:
  prometheus_data: { }
  grafana_data: { }
  postgres_data:
  logs_data:
  dags_data:
    driver_opts:
      type: none
      device: ../dags
      o: bind
  aws_config:
    driver_opts:
      type: none
      device: ${HOME}/.aws
      o: bind
  cassandra_data:
    driver: local
  cassandra2_data:
    driver: local

x-airflow-common:
  &airflow-common
  # In order to add custom dependencies or upgrade provider packages you can use your extended image.
  # Comment the image line, place your Dockerfile in the directory where you placed the docker-compose.yaml
  # and uncomment the "build" line below, Then run `docker-compose build` to build the images.
  image: ${AIRFLOW_IMAGE_NAME:-apache/airflow:2.2.2}
  build: .
  environment:
    &airflow-common-env
    AIRFLOW__CORE__EXECUTOR: CeleryExecutor
    AIRFLOW__CORE__SQL_ALCHEMY_CONN: postgresql+psycopg2://airflow:airflow@postgres/airflow
    AIRFLOW__CELERY__RESULT_BACKEND: db+postgresql://airflow:airflow@postgres/airflow
    AIRFLOW__CELERY__BROKER_URL: redis://:@redis:6379/0
    AIRFLOW__CORE__FERNET_KEY: ''
    AIRFLOW__CORE__DAGS_ARE_PAUSED_AT_CREATION: 'true'
    AIRFLOW__CORE__LOAD_EXAMPLES: 'false'
    AIRFLOW__API__AUTH_BACKEND: 'airflow.api.auth.backend.basic_auth'
    _PIP_ADDITIONAL_REQUIREMENTS: ${_PIP_ADDITIONAL_REQUIREMENTS:-}
  volumes:
    - aws_config:/opt/airflow/.aws:ro
    - dags_data:/opt/airflow/dags
    - ./logs:/opt/airflow/logs
    - ./plugins:/opt/airflow/plugins
  user: "${AIRFLOW_UID:-50000}:0"
  depends_on:
    &airflow-common-depends-on
    redis:
      condition: service_healthy
    postgres:
      condition: service_healthy



services:
  postgres:
    image: postgres:13
    environment:
      POSTGRES_USER: airflow
      POSTGRES_PASSWORD: airflow
      POSTGRES_DB: airflow
    volumes:
      - postgres_data:/var/lib/postgresql/data
    healthcheck:
      test: ["CMD", "pg_isready", "-U", "airflow"]
      interval: 5s
      retries: 5
    restart: always
    networks:
      - spark-network

  redis:
    image: redis:latest
    expose:
      - 6379
    healthcheck:
      test: ["CMD", "redis-cli", "ping"]
      interval: 5s
      timeout: 30s
      retries: 50
    restart: always
    networks:
      - spark-network

  airflow-webserver:
    <<: *airflow-common
    command: webserver
    ports:
      - 8080:8080
    healthcheck:
      test: ["CMD", "curl", "--fail", "http://localhost:8080/health"]
      interval: 10s
      timeout: 10s
      retries: 5
    restart: always
    depends_on:
      <<: *airflow-common-depends-on
      airflow-init:
        condition: service_completed_successfully
    networks:
      - spark-network

  airflow-scheduler:
    <<: *airflow-common
    command: scheduler
    healthcheck:
      test: ["CMD-SHELL", 'airflow jobs check --job-type SchedulerJob --hostname "$${HOSTNAME}"']
      interval: 10s
      timeout: 10s
      retries: 5
    restart: always
    depends_on:
      <<: *airflow-common-depends-on
      airflow-init:
        condition: service_completed_successfully
    networks:
      - spark-network

  airflow-worker:
    <<: *airflow-common
    command: celery worker
    healthcheck:
      test:
        - "CMD-SHELL"
        - 'celery --app airflow.executors.celery_executor.app inspect ping -d "celery@$${HOSTNAME}"'
      interval: 10s
      timeout: 10s
      retries: 5
    environment:
      <<: *airflow-common-env
      # Required to handle warm shutdown of the celery workers properly
      # See https://airflow.apache.org/docs/docker-stack/entrypoint.html#signal-propagation
      DUMB_INIT_SETSID: "0"
    restart: always
    depends_on:
      <<: *airflow-common-depends-on
      airflow-init:
        condition: service_completed_successfully
    networks:
      - spark-network

  airflow-triggerer:
    <<: *airflow-common
    command: triggerer
    healthcheck:
      test: ["CMD-SHELL", 'airflow jobs check --job-type TriggererJob --hostname "$${HOSTNAME}"']
      interval: 10s
      timeout: 10s
      retries: 5
    restart: always
    depends_on:
      <<: *airflow-common-depends-on
      airflow-init:
        condition: service_completed_successfully
    networks:
      - spark-network

  airflow-init:
    <<: *airflow-common
    entrypoint: /bin/bash
    # yamllint disable rule:line-length
    command:
      - -c
      - |
        function ver() {
          printf "%04d%04d%04d%04d" $${1//./ }
        }
        airflow_version=$$(gosu airflow airflow version)
        airflow_version_comparable=$$(ver $${airflow_version})
        min_airflow_version=2.2.0
        min_airflow_version_comparable=$$(ver $${min_airflow_version})
        if (( airflow_version_comparable < min_airflow_version_comparable )); then
          echo
          echo -e "\033[1;31mERROR!!!: Too old Airflow version $${airflow_version}!\e[0m"
          echo "The minimum Airflow version supported: $${min_airflow_version}. Only use this or higher!"
          echo
          exit 1
        fi
        if [[ -z "${AIRFLOW_UID}" ]]; then
          echo
          echo -e "\033[1;33mWARNING!!!: AIRFLOW_UID not set!\e[0m"
          echo "If you are on Linux, you SHOULD follow the instructions below to set "
          echo "AIRFLOW_UID environment variable, otherwise files will be owned by root."
          echo "For other operating systems you can get rid of the warning with manually created .env file:"
          echo "    See: https://airflow.apache.org/docs/apache-airflow/stable/start/docker.html#setting-the-right-airflow-user"
          echo
        fi
        one_meg=1048576
        mem_available=$$(($$(getconf _PHYS_PAGES) * $$(getconf PAGE_SIZE) / one_meg))
        cpus_available=$$(grep -cE 'cpu[0-9]+' /proc/stat)
        disk_available=$$(df / | tail -1 | awk '{print $$4}')
        warning_resources="false"
        if (( mem_available < 4000 )) ; then
          echo
          echo -e "\033[1;33mWARNING!!!: Not enough memory available for Docker.\e[0m"
          echo "At least 4GB of memory required. You have $$(numfmt --to iec $$((mem_available * one_meg)))"
          echo
          warning_resources="true"
        fi
        if (( cpus_available < 2 )); then
          echo
          echo -e "\033[1;33mWARNING!!!: Not enough CPUS available for Docker.\e[0m"
          echo "At least 2 CPUs recommended. You have $${cpus_available}"
          echo
          warning_resources="true"
        fi
        if (( disk_available < one_meg * 10 )); then
          echo
          echo -e "\033[1;33mWARNING!!!: Not enough Disk space available for Docker.\e[0m"
          echo "At least 10 GBs recommended. You have $$(numfmt --to iec $$((disk_available * 1024 )))"
          echo
          warning_resources="true"
        fi
        if [[ $${warning_resources} == "true" ]]; then
          echo
          echo -e "\033[1;33mWARNING!!!: You have not enough resources to run Airflow (see above)!\e[0m"
          echo "Please follow the instructions to increase amount of resources available:"
          echo "   https://airflow.apache.org/docs/apache-airflow/stable/start/docker.html#before-you-begin"
          echo
        fi
        mkdir -p /sources/logs /sources/dags /sources/plugins
        chown -R "${AIRFLOW_UID}:0" /sources/{logs,dags,plugins}
        exec /entrypoint airflow version
    # yamllint enable rule:line-length
    environment:
      <<: *airflow-common-env
      _AIRFLOW_DB_UPGRADE: 'true'
      _AIRFLOW_WWW_USER_CREATE: 'true'
      _AIRFLOW_WWW_USER_USERNAME: ${_AIRFLOW_WWW_USER_USERNAME:-airflow}
      _AIRFLOW_WWW_USER_PASSWORD: ${_AIRFLOW_WWW_USER_PASSWORD:-airflow}
    user: "0:0"
    volumes:
      - .:/sources
    networks:
      - spark-network

  airflow-cli:
    <<: *airflow-common
    profiles:
      - debug
    environment:
      <<: *airflow-common-env
      CONNECTION_CHECK_MAX_COUNT: "0"
    # Workaround for entrypoint issue. See: https://github.com/apache/airflow/issues/16252
    command:
      - bash
      - -c
      - airflow
    networks:
      - spark-network

  flower:
    <<: *airflow-common
    command: celery flower
    ports:
      - 5555:5555
    healthcheck:
      test: ["CMD", "curl", "--fail", "http://localhost:5555/"]
      interval: 10s
      timeout: 10s
      retries: 5
    restart: always
    depends_on:
      <<: *airflow-common-depends-on
      airflow-init:
        condition: service_completed_successfully
    networks:
      - spark-network

  # zookeep1:
  #   image: zookeeper:3.4.14
  #   hostname: zookeep1
  #   ports:
  #     - "2181:2181"
  #   environment:
  #       ZOO_MY_ID: 1
  #       ZOO_PORT: 2181
  #       ZOO_SERVERS: server.1=zookeep1:2888:3888 server.2=zookeep2:2888:3888 server.3=zookeep3:2888:3888
  #   volumes:
  #     - ./zk-multiple-kafka-multiple/zookeep1/data:/data
  #     - ./zk-multiple-kafka-multiple/zookeep1/datalog:/datalog
  #   networks: 
  #     - spark-network

  # zookeep2:
  #   image: zookeeper:3.4.14
  #   hostname: zookeep2
  #   ports:
  #     - "2182:2182"
  #   environment:
  #       ZOO_MY_ID: 2
  #       ZOO_PORT: 2182
  #       ZOO_SERVERS: server.1=zookeep1:2888:3888 server.2=zookeep2:2888:3888 server.3=zookeep3:2888:3888
  #   volumes:
  #     - ./zk-multiple-kafka-multiple/zookeep2/data:/data
  #     - ./zk-multiple-kafka-multiple/zookeep2/datalog:/datalog
  #   networks: 
  #     - spark-network

  # zookeep3:
  #   image: zookeeper:3.4.14
  #   hostname: zookeep3
  #   ports:
  #     - "2183:2183"
  #   environment:
  #       ZOO_MY_ID: 3
  #       ZOO_PORT: 2183
  #       ZOO_SERVERS: server.1=zookeep1:2888:3888 server.2=zookeep2:2888:3888 server.3=zookeep3:2888:3888
  #   volumes:
  #     - ./zk-multiple-kafka-multiple/zookeep3/data:/data
  #     - ./zk-multiple-kafka-multiple/zookeep3/datalog:/datalog
  #   networks: 
  #     - spark-network


  # kafka1:
  #   image: confluentinc/cp-kafka:5.2.1
  #   hostname: kafka1
  #   ports:
  #     - "9092:9092"
  #   environment:
  #     KAFKA_ADVERTISED_LISTENERS: LISTENER_DOCKER_INTERNAL://kafka1:19092,LISTENER_DOCKER_EXTERNAL://${DOCKER_HOST_IP:-127.0.0.1}:9092
  #     KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: LISTENER_DOCKER_INTERNAL:PLAINTEXT,LISTENER_DOCKER_EXTERNAL:PLAINTEXT
  #     KAFKA_INTER_BROKER_LISTENER_NAME: LISTENER_DOCKER_INTERNAL
  #     KAFKA_ZOOKEEPER_CONNECT: "zookeep1:2181,zookeep2:2182,zookeep3:2183"
  #     KAFKA_BROKER_ID: 1
  #     KAFKA_LOG4J_LOGGERS: "kafka.controller=INFO,kafka.producer.async.DefaultEventHandler=INFO,state.change.logger=INFO"
  #   volumes:
  #     - ./zk-multiple-kafka-multiple/kafka1/data:/var/lib/kafka/data
  #   depends_on:
  #     - zookeep1
  #     - zookeep2
  #     - zookeep3
  #   networks: 
  #     - spark-network

  # kafka2:
  #   image: confluentinc/cp-kafka:5.2.1
  #   hostname: kafka2
  #   ports:
  #     - "9093:9093"
  #   environment:
  #     KAFKA_ADVERTISED_LISTENERS: LISTENER_DOCKER_INTERNAL://kafka2:19093,LISTENER_DOCKER_EXTERNAL://${DOCKER_HOST_IP:-127.0.0.1}:9093
  #     KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: LISTENER_DOCKER_INTERNAL:PLAINTEXT,LISTENER_DOCKER_EXTERNAL:PLAINTEXT
  #     KAFKA_INTER_BROKER_LISTENER_NAME: LISTENER_DOCKER_INTERNAL
  #     KAFKA_ZOOKEEPER_CONNECT: "zookeep1:2181,zookeep2:2182,zookeep3:2183"
  #     KAFKA_BROKER_ID: 2
  #     KAFKA_LOG4J_LOGGERS: "kafka.controller=INFO,kafka.producer.async.DefaultEventHandler=INFO,state.change.logger=INFO"
  #   volumes:
  #     - ./zk-multiple-kafka-multiple/kafka2/data:/var/lib/kafka/data
  #   depends_on:
  #     - zookeep1
  #     - zookeep2
  #     - zookeep3
  #   networks: 
  #     - spark-network

  # kafka3:
  #   image: confluentinc/cp-kafka:5.2.1
  #   hostname: kafka3
  #   ports:
  #     - "9094:9094"
  #   environment:
  #     KAFKA_ADVERTISED_LISTENERS: LISTENER_DOCKER_INTERNAL://kafka3:19094,LISTENER_DOCKER_EXTERNAL://${DOCKER_HOST_IP:-127.0.0.1}:9094
  #     KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: LISTENER_DOCKER_INTERNAL:PLAINTEXT,LISTENER_DOCKER_EXTERNAL:PLAINTEXT
  #     KAFKA_INTER_BROKER_LISTENER_NAME: LISTENER_DOCKER_INTERNAL
  #     KAFKA_ZOOKEEPER_CONNECT: "zookeep1:2181,zookeep2:2182,zookeep3:2183"
  #     KAFKA_BROKER_ID: 3
  #     KAFKA_LOG4J_LOGGERS: "kafka.controller=INFO,kafka.producer.async.DefaultEventHandler=INFO,state.change.logger=INFO"
  #   volumes:
  #     - ./zk-multiple-kafka-multiple/kafka3/data:/var/lib/kafka/data
  #   depends_on:
  #     - zookeep1
  #     - zookeep2
  #     - zookeep3
  #   networks: 
  #     - spark-network

  #Spark with 3 workers
  # spark-master:
  #   image: andreper/spark-master:3.0.0
  #   container_name: spark-master
  #   ports:
  #     - "8181:8080"
  #     - "7077:7077"
  #   volumes:
  #     - ${HOME}/.aws:/home/airflow/.aws:ro
  #     - dags_data:/opt/airflow/dags
  #   networks: 
  #     spark-network:

  # spark-worker-1:
  #   image: andreper/spark-worker:3.0.0
  #   container_name: spark-worker-1
  #   environment:
  #     - SPARK_WORKER_CORES=1
  #     - SPARK_WORKER_MEMORY=512m
  #   ports:
  #     - "8081:8081"
  #   volumes:
  #     - ${HOME}/.aws:/home/airflow/.aws:ro
  #     - ../dags:/opt/airflow/dags
  #   depends_on:
  #     - spark-master
  #   networks: 
  #     spark-network:

  # spark-worker-2:
  #   image: andreper/spark-worker:3.0.0
  #   container_name: spark-worker-2
  #   environment:
  #     - SPARK_WORKER_CORES=1
  #     - SPARK_WORKER_MEMORY=512m
  #   ports:
  #     - "8082:8081"
  #   volumes:
  #     - ${HOME}/.aws:/home/airflow/.aws:ro
  #     - ../dags:/opt/airflow/dags
  #   depends_on:
  #     - spark-master
  #   networks: 
  #     spark-network:
  spark-master:
    image: docker.io/bitnami/spark:3.2.0 #bitnami/spark/kafka/cassandra:3.1 #docker.io/bitnami/spark:3
    user: root # Run container as root container: https://docs.bitnami.com/tutorials/work-with-non-root-containers/
    container_name: spark-master
    environment:
      - SPARK_MODE=master
    volumes:
      - ${HOME}/.aws:/home/airflow/.aws:ro
      - ../dags:/opt/airflow/dags
      #- /opt/spark/jars:/opt/bitnami/spark/jars
    ports:
      - "8181:8080" # Master UI
      - "7077:7077"
    networks: 
      spark-network:
        # ipv4_address: 10.5.0.10

  spark-worker-1:
    image: docker.io/bitnami/spark:3.2.0 #bitnami/spark/kafka/cassandra:3.1 #docker.io/bitnami/spark:3
    user: root
    container_name: spark-worker-1
    environment:
      - SPARK_MODE=worker
      - SPARK_MASTER_URL=spark://spark-master:7077
      - SPARK_WORKER_MEMORY=512m
      - SPARK_WORKER_CORES=1
    volumes:
      - ${HOME}/.aws:/home/airflow/.aws:ro
      - dags_data:/opt/airflow/dags
      #- /opt/spark/jars:/opt/bitnami/spark/jars
    depends_on:
      - spark-master
    networks: 
        spark-network:
          # ipv4_address: 10.5.0.11
  spark-worker-2:
    image: docker.io/bitnami/spark:3.2.0 #bitnami/spark/kafka/cassandra:3.1 #docker.io/bitnami/spark:3
    user: root
    container_name: spark-worker-2
    environment:
      - SPARK_MODE=worker
      - SPARK_MASTER_URL=spark://spark-master:7077
      - SPARK_WORKER_MEMORY=512m
      - SPARK_WORKER_CORES=1
    volumes:
      - ${HOME}/.aws:/home/airflow/.aws:ro
      - dags_data:/opt/airflow/dags
      #- /opt/spark/jars:/opt/bitnami/spark/jars
    depends_on:
      - spark-master
    networks: 
        spark-network:
          #ipv4_address: 10.5.0.12

  # spark-worker-3:
  #   image: bitnami/spark/kafka/cassandra:3.1 #docker.io/bitnami/spark:3
  #   user: root
  #   container_name: spark-worker-3
  #   hostname: spark-worker-3
  #   environment:
  #     - SPARK_MODE=worker
  #     - SPARK_MASTER_URL=spark://spark:7077
  #     - SPARK_WORKER_MEMORY=1G
  #     - SPARK_WORKER_CORES=1
  #     - SPARK_RPC_AUTHENTICATION_ENABLED=no
  #     - SPARK_RPC_ENCRYPTION_ENABLED=no
  #     - SPARK_LOCAL_STORAGE_ENCRYPTION_ENABLED=no
  #     - SPARK_SSL_ENABLED=no
  #   volumes:
  #     - ${HOME}/.aws:/home/airflow/.aws:ro
  #     - dags_data:/opt/airflow/dags
  #     #- /opt/spark/jars:/opt/bitnami/spark/jars
  #   depends_on:
  #     - spark-master
  #   networks: 
  #       spark-network:
  #         ipv4_address: 10.5.0.13

  #Jupyter notebook
  jupyter-spark:
    image: jupyter/pyspark-notebook:spark-3.2.0
    ports:
      - "8888:8888"
      - "4040-4050:4040-4050"
    environment:
      - JUPYTER_ENABLE_LAB=yes # To turn on Jupyter_Lab
    volumes:
      - ${HOME}/.aws:/home/airflow/.aws:ro
      - ../dags/src:/home/jovyan/work/notebooks/
    networks: 
      spark-network:
        # ipv4_address: 10.5.0.15

networks:
  spark-network:
    driver: bridge
    ipam:
     driver: default
    #  config:
    #    - subnet: 10.5.0.0/16
    #    - gateway: 10.5.0.1/16
  # cassandra:
  #   image: docker.io/bitnami/cassandra:4.0
  #   ports:
  #     - 7000:7000
  #     - 9042:9042
  #   volumes:
  #     - cassandra_data:/bitnami
  #   environment:
  #     - CASSANDRA_SEEDS=cassandra,cassandra2
  #     - CASSANDRA_CLUSTER_NAME=cassandra-cluster
  #     - CASSANDRA_PASSWORD_SEEDER=no
  #     - CASSANDRA_PASSWORD=cassandra
  #     # By default, Cassandra autodetects the available host memory and takes as much as it can.
  #     # Therefore, memory options are mandatory if multiple Cassandras are launched in the same node.
  #     - MAX_HEAP_SIZE=2G
  #     - HEAP_NEWSIZE=200M
  #   networks: 
  #     spark-network:
  #       ipv4_address: 10.5.0.16
  # cassandra2:
  #   image: docker.io/bitnami/cassandra:4.0
  #   ports:
  #     - 7001:7000
  #     - 9043:9042
  #   volumes:
  #     - cassandra2_data:/bitnami
  #   environment:
  #     - CASSANDRA_SEEDS=cassandra,cassandra2
  #     - CASSANDRA_CLUSTER_NAME=cassandra-cluster
  #     - CASSANDRA_PASSWORD=cassandra
  #     # By default, Cassandra autodetects the available host memory and takes as much as it can.
  #     # Therefore, memory options are mandatory if multiple Cassandras are launched in the same node.
  #     - MAX_HEAP_SIZE=2G
  #     - HEAP_NEWSIZE=200M
  #   networks: 
  #     spark-network:
  #       ipv4_address: 10.5.0.17

# networks:
#   spark-network:
#     driver: bridge
#     ipam:
#      driver: default
#      config:
#        - subnet: 10.5.0.0/16
