Feature: I want to be sure that data is transform for user and tweet correctly. 
  
  Scenario: Transform user twitter data   
    Given The json file from twitter
    When the transform_user_data function was invoked
    Then the dataframe transform_user_data should "successfully" compared with
    |user_id               |name     |followers_count|friends_count|location|
    |1314164146934513665   |Whopcod  |313            |1            |Turkey  |
  
  Scenario: Transform tweet twitter data   
    Given The json file from twitter 
    When the transform_tweet_data function was invoked  
    Then the dataframe transform_tweet_data should "successfully" compared with
    |timestamp                   |tweet_id           |retweet_count|favorite_count|text                                                                                   |    
    | 2021-12-07 21:29:35        |1468331881146028043|19           |9             |Niche Marketing Kit https://t.co/6yty0x4X1O  #MachineLearning #Python #JS #Java #DevOps | 