#! bin/bash

# Start master node of standalone spark cluster
. start-master.sh

# Start worker node of standalone spark cluster
. start-worker.sh spark://localhost:7077