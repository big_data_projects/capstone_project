from behave import given,when,then # pylint: disable=no-name-in-module,import-error
from pyspark.sql.types import StructType,StringType, StructField, IntegerType,LongType
import os,sys
dir = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
sys.path.append(os.path.join(dir,"dags"))
from dags.src.spark.scripts.spark_transformation import transform_user_data, transform_tweet_data # pylint: disable=import-error

@given('The json file from twitter')
def step_impl(context): # pylint: disable= missing-function-docstring
    # context.raw = context.spark.read.json(f"{DATA_JSON_DIR}/tweet_20211207222940523927.json")
    innerStruct = StructType([StructField('created_at',StringType()),
                     StructField('tweet_id',LongType()),
                     StructField('retweetcount',IntegerType()),
                     StructField('favorite_count',IntegerType()),
                     StructField('tweet',StringType()),
                     StructField('place',StringType()),
                     StructField('user_id',LongType()),
                     StructField('user_name',StringType()),
                     StructField('followers_count',IntegerType()),
                     StructField('friends_count',IntegerType()),
                     StructField('location',StringType())])
    context.schema = StructType([StructField("json",innerStruct,True)])
    context.raw = [(("2021-12-07 21:29:35",1468331881146028043,19,9,
                    "Niche Marketing Kit https://t.co/6yty0x4X1O  #MachineLearning #Python #JS #Java #DevOps",
                    "test_place",1314164146934513665,"Whopcod",313,1,"Turkey"),)]
    
    context.raw =context.spark.createDataFrame(context.raw,context.schema)


@when('the transform_user_data function was invoked')
def step_impl(context): # pylint: disable= function-redefined,missing-function-docstring

    context.df_transform_user_data = transform_user_data(context.raw)


@then('the dataframe transform_user_data should "{comparison_result}" compared with')
def step_impl(context,comparison_result): # pylint: disable= function-redefined,missing-function-docstring
    context.schema_user = (StructType([StructField('user_id',LongType()),
                    StructField('name',StringType()),
                    StructField('followers_count',IntegerType()),
                    StructField('friends_count',IntegerType()),
                    StructField('location',StringType())]))


    data=[]
    for row in context.table:
        data.append((int(row["user_id"]),row["name"],int(row["followers_count"]),
                    int(row["friends_count"]),row["location"]))
    select_col = ["user_id","name","followers_count","friends_count","location"]
    orde_by_col = ["user_id","name"]

    context.df_transform_user_data_result = context.spark.createDataFrame(data,context.schema_user)\
                                            .select(select_col) \
                                            .orderBy(orde_by_col)
    context.df_transform_user_data = context.df_transform_user_data \
                                            .select(select_col) \
                                            .orderBy(orde_by_col)
    print(context.df_transform_user_data_result.printSchema())
    print(context.df_transform_user_data.printSchema())

    print(context.df_transform_user_data_result.show())
    print(context.df_transform_user_data.show())
    context.comparison_result = context.df_transform_user_data_result.toPandas() \
                            .equals(context.df_transform_user_data.toPandas())

    if comparison_result == "successfully":
        assert context.comparison_result is True
    else:
        assert context.comaparison_result is False

@when('the transform_tweet_data function was invoked')
def step_impl(context): # pylint: disable= function-redefined,missing-function-docstring

    context.df_transform_tweet_data = transform_tweet_data(context.raw)


@then('the dataframe transform_tweet_data should "{comparison_result}" compared with')
def step_impl(context,comparison_result): # pylint: disable= function-redefined,missing-function-docstring
    context.schema_tweet = StructType([StructField('timestamp',StringType()),
                        StructField('tweet_id',LongType()),
                        StructField('retweet_count',IntegerType()),
                        StructField('favorite_count',IntegerType()),
                        StructField('text',StringType()),
                        ])


    data=[]
    for row in context.table:
        data.append((row["timestamp"],int(row["tweet_id"]),int(row["retweet_count"]),
                    int(row["favorite_count"]),row["text"]))
    select_col = ["timestamp","tweet_id","retweet_count","favorite_count","text"]
    orde_by_col = ["timestamp","tweet_id"]

    context.df_transform_tweet_data_result = context.spark.createDataFrame(data,context.schema_tweet)\
                                            .select(select_col) \
                                            .orderBy(orde_by_col)
    context.df_transform_tweet_data = context.df_transform_tweet_data \
                                            .select(select_col) \
                                            .orderBy(orde_by_col)
    print(context.df_transform_tweet_data_result.printSchema())
    print(context.df_transform_tweet_data.printSchema())

    print(context.df_transform_tweet_data_result.show())
    print(context.df_transform_tweet_data.show())

    context.comparison_result = context.df_transform_tweet_data_result.toPandas() \
                            .equals(context.df_transform_tweet_data.toPandas())

    if comparison_result == "successfully":
        assert context.comparison_result is True
    else:
        assert context.comaparison_result is False

