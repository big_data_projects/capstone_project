
import sys
import os
from behave import *
from pyspark.sql import SparkSession  # pylint: disable=import-error
sys.path.append('/home/tmatczak/Desktop/python/capstone_project/dags/src')
sys.path.append('/opt/airflow/dags/src')
dir = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
sys.path.append(os.path.join(dir,"src"))
# Start standalone spark
#  . start-master.sh => UI localhost:8182
#  . start-worker.sh spark://localhost:7077
# . start-worker.sh spark://tmatczak-VirtualBox:7077 
def before_all(context):
    context.spark = SparkSession \
                    .builder.master("spark://tmatczak-VirtualBox:7077") \
                    .appName("behave-with-spark") \
                    .getOrCreate()


def after_all(context):
    if 'spark' in context:
        context.spark.sparkContext.stop()